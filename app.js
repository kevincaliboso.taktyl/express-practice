const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

app.use(cors())
app.use(bodyParser.json());

const postsRoute = require('./routes/posts');

app.use('/api/posts', postsRoute);

// Connect to mongoDB
mongoose.connect(process.env.DB_URL, () => {
    console.log("Connected to MongoDB");
});

app.listen(3000, () => {
    console.log('Server started on port 3000');
});

