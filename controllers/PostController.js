'use strict';

const Post = require('../models/Post');

const getPosts = (async (req, res) => {
    try {
        const posts = await Post.find();
        res.status(200).json({
            message : "Here is all the available post.",
            data : posts
        });
    } catch (error) {
        res.status(400).json({ message: error });
    }
})

const getPost = (async (req, res) => {
    const { id } = req.params;

    try {
        const post = await Post.findById(id);

        if (!post) {
            return res.status(404).json({message : 'ID does not exist.'})
        }

        res.status(200).json({
            message : `Here is the content of post ${id}`,
            data : post
        })
    } catch (error) {
        res.status(400).json({ message: error });
    }
})

const createPost = (async (req, res) => {
    const {title, description} = req.body;

    if (!title || !description) {
        return res.status(400).json({message: 'Please check your inputs. Make sure you have title and description.'});
    }

    const post = new Post({
        title,
        description
    });

    try {
        const savedPost = await post.save();
        res.status(200).json({
            message : "Created post.",
            data : savedPost
        });
    } catch (error) {
        res.status(400).json({ message: error });
    }
})

const updatePost = (async (req, res) => {
    const { id } = req.params;
    const {title, description} = req.body;

    if (!title || !description) {
        return res.status(400).send('Please check your inputs. Make sure you have title and description.');
    }

    try {
        const updatedPost = await Post.findById(id)
        updatedPost.title = title
        updatedPost.description = description
        updatedPost.save()
        res.status(200).json({
            message : `Successfully updated post ${id}`,
            data : updatedPost
        })
    } catch (error) {
        res.status(400).json({ message: error });
    }
})

const deletePost = (async (req, res) => {
    const { id } = req.params;

    try {
        await Post.remove({ _id: id })
        res.status(200).json({
            message : `Successfully deleted post`
        })
    } catch (error) {
        res.status(400).json({ message: error });
    }
})

module.exports = {
    getPosts,
    getPost,
    createPost,
    updatePost,
    deletePost
}