### `dev`

Start this application on development environment. Install nodemon on your device.

```
npm run dev
# or
yarn dev
```

### `start`

Start this application in production mode.

```
npm run start
# or
yarn start
```

## ✨ What's done inside the project

- MongoDB connection using mongoose
- CRUD for a model called "post" that is connected to a test account on MongoDB
- Setup of Express and other essential packages to make the API server more dynamic (cors, body-parser, lodash)
- Basic routing and foldering to make the app.js clean.

## 📔 TODO

- Try PostgreSQL or MySQL on this build.
- Add security via Bearer Token for a client device.

---
